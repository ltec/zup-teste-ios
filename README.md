# Zup Teste iOS


## Desenvolvimento

Desenvolver um app iOS para fins de avaliação seguindo os passos das próximas sessões e claro mostrar sua criatividade.

## Passos

- Criar um branch apartir da branch `develop`
- Criar um projeto do zero e subir o código nessa branch
- Criar telas (não precisa ser igual, use sua criatividade) conforme na sessão de `Telas`

## Telas

### Home

![home](home.png)

### Detail

![detail](detail.png)

## Requisitos do teste

- Utilizar API `https://developer.marvel.com/documentation/getting_started`
- Utilizar Arquitetura MVVM
- Utilizar Coordinator
- Utilizar reutilização de código (componentes, fontes, size, bases e protocols)
- Cobertura de teste de pelo menos 70%
- Utilizar testes de snapshot
- Documentar projeto no Readme (componentes disponiveis, como rodar os testes, telas, etc...)
- Utilizar `.xib` ou `viewcode`
- Desenvolver o app de forma acessível
- Controle de estado (`carregando`, `sucesso`, `vazio` e `erro`) das telas

## Adicionais (Opcionais)

- Splash Screen
- Recursos nativos (como printar e compartilhar a tela pelo whatsapp por exemplo)
- Separar por PODs
- Design Patterns
- Injeção de Dependência
